package nl.sogeti.demo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * The JPA entity class that models the domain object Person. The primary key
 * (id) is generated.
 * 
 * @author Erwin
 * 
 */
@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 982374691237864L;

    @Id
    @GeneratedValue
    private Long id;
    
    
    @Column(scale = 2, precision = 3)
    private BigDecimal price;
    
    @NotNull
    @Column(updatable=false,unique=true)
    private String productId;
    
    private String name;

    public Product(){
	this.productId=UUID.randomUUID().toString();
    }
    
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public BigDecimal getPrice() {
	return price;
    }
    
    public BigDecimal getTotalPrice(int quantity) {
	if(this.getPrice()==null) return BigDecimal.ZERO; 
	return BigDecimal.valueOf(quantity).multiply((this.getPrice()));
    }

    public void setPrice(BigDecimal price) {
	this.price = price;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((productId == null) ? 0 : productId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) return true;
	if (obj == null) return false;
	if (getClass() != obj.getClass()) return false;
	Product other = (Product) obj;
	if (productId == null) {
	    if (other.productId != null) return false;
	} else if (!productId.equals(other.productId)) return false;
	return true;
    }


    
    

}
