package nl.sogeti.demo.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class ShoppingBasket {

    private Map<Product,Integer> products = new HashMap<>();
    
    public void addProduct(Product product){
	Integer quantity = products.get(product);
	if(quantity == null){
	    quantity =0;
	}
	products.put(product, ++quantity);
    }
    
    public List<Entry<Product,Integer>> getProducts(){
	return new ArrayList<Entry<Product, Integer>>(this.products.entrySet());
    }
    
    public void removeProduct(Product product){
	if(products.containsKey(product)){
	    int quantity = products.get(product);
	    if(quantity == 1){
		products.remove(product);
	    } else {
		products.put(product, --quantity);
	    }
	}
    }
    
    public int getSize(){
	int size = 0;
	for(Entry<Product, Integer> entry : products.entrySet()){
	    size +=entry.getValue();
	}
	return size;
    }
    
    public BigDecimal getOrderTotal(){
	BigDecimal total = BigDecimal.valueOf(0, 2);
	for(Entry<Product, Integer> entry : products.entrySet()){
	    total=total.add(entry.getKey().getTotalPrice(entry.getValue()));
	}
	return total;
    }
}
