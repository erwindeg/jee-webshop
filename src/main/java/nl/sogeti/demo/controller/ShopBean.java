package nl.sogeti.demo.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import nl.sogeti.demo.model.Product;
import nl.sogeti.demo.model.ShoppingBasket;
import nl.sogeti.demo.service.CrudService;

/**
 * This is the JSF managed bean that handles calls from the home.xhtml This
 * class uses the PersonService EJB, injected by the container by use of the @Inject
 * annotation.
 * 
 * @author Erwin
 * 
 */
@ManagedBean
@SessionScoped
public class ShopBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    Logger logger;

    @Inject
    CrudService crudService;
    
    private ShoppingBasket shoppingBasket;
    
    public ShopBean(){
	this.shoppingBasket = new ShoppingBasket();
    }
    
    public ShoppingBasket getShoppingBasket(){
	return this.shoppingBasket;
    }
    

    public List<Product> list() {
	return crudService.findAll(Product.class);
    }

}