/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.sogeti.demo.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author giererwi
 * @param <T>
 *            The entity to use the CRUD on.
 */

@Stateless
public class CrudService
{

    @PersistenceContext
   EntityManager entityManager;

   public void setEntityManager(EntityManager entityManager)
   {
      this.entityManager = entityManager;
   }

   public <T> T persist(T entity)
   {
      this.entityManager.persist(entity);
      return entity;
   }

   public <T> T merge(T entity)
   {
      return this.entityManager.merge(entity);
   }

   public <T> void remove(T entity)
   {
      T attached = this.entityManager.merge(entity);
      this.entityManager.remove(attached);
   }

   public <T> T find(Class<T> entity, Long key)
   {
      return this.entityManager.find(entity, key);
   }

   public <T> T find(Class<T> entity, String key)
   {
      return this.entityManager.find(entity, key);
   }

   @SuppressWarnings("rawtypes")
   public <T> T find(Class<T> entity, Enum key)
   {
      return this.entityManager.find(entity, key);
   }

   public <T> List<T> findAll(Class<T> entity)
   {
      CriteriaQuery<T> cq = entityManager.getCriteriaBuilder().createQuery(entity);
      Root<T> entityRoot = cq.from(entity);
      cq.select(entityRoot);
      TypedQuery<T> query = entityManager.createQuery(cq);
      return query.getResultList();
   }

   public <T> T findByProperty(Class<T> entity, String property, Object value)
   {
      CriteriaBuilder cb = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> cq = cb.createQuery(entity);
      Root<T> entityRoot = cq.from(entity);
      Predicate equalsProperty = cb.equal(entityRoot.get(property), value);
      cq.where(equalsProperty);
      TypedQuery<T> query = entityManager.createQuery(cq);
      return query.getSingleResult();
   }

   public <T> List<T> listByProperty(Class<T> entity, String property, Object value)
   {
      CriteriaBuilder cb = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> cq = cb.createQuery(entity);
      Root<T> entityRoot = cq.from(entity);
      Predicate equalsProperty = cb.equal(entityRoot.get(property), value);
      cq.where(equalsProperty);
      TypedQuery<T> query = entityManager.createQuery(cq);

      return query.getResultList();
   }

}
