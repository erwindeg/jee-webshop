package nl.sogeti.demo.service;

import java.math.BigDecimal;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import nl.sogeti.demo.model.Product;

@Singleton
@Startup
public class ApplicationStartupService
{

   @Inject
   CrudService crudService;

   @Inject
   Logger logger;

   @PostConstruct
   void init()
   {
      logger.info("Creating application startup data");
     
      for(int i = 0;i<100;i++){
	  Product product = new Product();
	  product.setName("name"+i);
	  product.setPrice(BigDecimal.valueOf(Math.random()*10));
	  crudService.persist(product);
      }
   }
}
